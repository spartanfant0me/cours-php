<?php

class Personne {

    public string $prenom;

    public function __construct(string $prenom){
        $this->prenom = $prenom;
    }

    public static function compare(Personne $a, Personne $b){
        return $a->prenom <=> $b->prenom;
        /*if($a->prenom > $b->prenom){
            return 1;
        }
        if($a->prenom < $b->prenom){
            return -1;
        }
        return 0;*/
    }

}
