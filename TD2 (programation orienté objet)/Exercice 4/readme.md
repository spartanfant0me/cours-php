## Exercice 4 :

### Question :
Écrivez une classe Personne avec un prénom puis une classe qui trie un tableau de
personnes en fonction du prénom. Vous pouvez vous aider du tableau ci-dessous
````php
$prenoms = [
"Louis",
"Sylvain",
"Antoine",
"Léon",
"Colin",
"Christine",
"Nathalie",
"Sylvie",
"François",
"Guy",
"Vincent",
"Alain",
"Serge",
"Gérard",
"Bernard",
"Léa",
];
````
