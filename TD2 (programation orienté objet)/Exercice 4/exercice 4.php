<?php

include 'Personne.php';
include 'PersonneHelper.php';

$prenoms = [
    "Louis",
    "Sylvain",
    "Antoine",
    "Léon",
    "Colin",
    "Christine",
    "Nathalie",
    "Sylvie",
    "François",
    "Guy",
    "Vincent",
    "Alain",
    "Serge",
    "Gérard",
    "Bernard",
    "Léa",
];

main();

function main()
{
    global $prenoms;
    $personnes = PersonneHelper::createFromPrenoms($prenoms);
    PersonneHelper::affiche($personnes);
    PersonneHelper::sort($personnes);
    echo "----------\n";
    PersonneHelper::affiche($personnes);
}
