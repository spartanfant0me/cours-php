<?php

class PersonneHelper {

    public static function sort(array &$personnes){
        usort($personnes, 'Personne::compare');
    }

    public static function affiche(array $personnes){
        foreach($personnes as $personne){
            echo $personne->prenom."\n";
        }
    }

    public static function createFromPrenoms(array $prenoms) : array {
        $personnes = [];
        foreach($prenoms as $prenom){
            $personnes[] = new Personne($prenom);
        }
        return $personnes;
    }

}
