<?php
class Note {

    public Etudiant $e;
    public Professeur $p;
    public int $note;

    public function __construct(Etudiant $e, Professeur $p, int $note){
        $this->e = $e;
        $this->p = $p;
        $this->note = $note;
    }

}
