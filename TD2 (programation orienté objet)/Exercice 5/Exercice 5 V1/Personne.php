<?php
class Personne {

    public string $nom, $prenom;
    public int $age;

    public function __construct(string $nom, string $prenom, int $age){
        $this->prenom = $prenom;
        $this->nom = $nom;
        $this->age = $age;
    }

}
