## Exercice 5 :

Écrivez une classe « étudiant » qui possède :
- Un nom
- Un prénom
- Un âge
- Une promotion

Écrivez une classe « professeur » qui possède :
- Un nom
- Un prénom
- Un âge
- Une matière enseignée

### Questions :

1. Proposez un modèle objet puis implémentez-le
2. Proposez une façon d’implémenter une note par matière
