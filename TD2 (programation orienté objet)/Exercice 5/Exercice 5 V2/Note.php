<?php
class Note {

    public Professeur $p;
    public float $note;

    public function __construct(Professeur $p, float $note){
        $this->p = $p;
        $this->note = $note;
    }

}
