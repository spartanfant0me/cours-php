<?php

class Bulletin
{
    public array $notes = [];
    public Etudiant $e;
    public function __construct(Etudiant $e){
        $this->e = $e;
    }

    public function addNoteByInt(int $note, Professeur $p){
        $this->notes[] = new Note($p, $note);
    }

    public function addNoteByNote(Note $note): void{
        $this->notes[] = $note;
    }

    public function getMoyenneGenerale(): float{
        $moyenne = 0;
        foreach ($this->notes as $note) {
            $moyenne += $note->note;
        }
        return round($moyenne / sizeof($this->notes), 2);

    }

    public function getMatieres(): array{
        $matieres = [];
        foreach ($this->notes as $note) {
            if (in_array($note->p->matiere, $matieres)) {
                continue;
            }
            $matieres[] = $note->p->matiere;
        }
        return $matieres;
    }

}
