## Exercice 5 :

Écrivez une classe « étudiant » qui possède :
- Un nom
- Un prénom
- Un âge
- Une promotion

Écrivez une classe « professeur » qui possède :
- Un nom
- Un prénom
- Un âge
- Une matière enseignée

### Questions :
Proposez une façon d’implémenter plusieurs notes par matière
