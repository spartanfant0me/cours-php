<?php

include "Personne.php";
include "Etudiant.php";
include "Professeur.php";
include "Note.php";
include "Bulletin.php";

main();

function main(){
    $e = new Etudiant("john", "wick", 12, "L3");
    $p = new Professeur("Zen", "Yataa", 120, "Prog");
    $p1 = new Professeur("Gen", "ji", 30, "Rezo");
    $b = new Bulletin($e);
    $b->addNoteByInt(5, $p);
    $b->addNoteByInt(15, $p);
    $b->addNoteByInt(20, $p1);
    var_dump($b->getMatieres());
    var_dump($b->getMoyenneGenerale());
}
