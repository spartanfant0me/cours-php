<?php

class TriLeTableau{

    public array $tab;

    public function __construct(array $tab){
        $this->tab = $tab;
    }

    public function tri(){
        sort($this->tab);
        return $this;
    }

    public function affiche(){
        for ($i = 0; $i < sizeof($this->tab); $i++) {
            echo $i . ' : ' . $this->tab[$i] . "\n";
        }
        return $this;
    }

    public function elSeparator(){
        echo "--------\n";
        return $this;
    }
}

$leTableau = [4, 23, 45, 1, 2];
$tlt = new TriLeTableau($leTableau);
$tlt->affiche()->tri()->elSeparator()->affiche();
