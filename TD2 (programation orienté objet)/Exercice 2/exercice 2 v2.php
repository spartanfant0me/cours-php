<?php

class TriLeTableau{
    public function tri(array &$tab){
        sort($tab);
    }

    public function affiche(array &$tab){
        for ($i = 0; $i < sizeof($tab); $i++) {
            echo $i . ' : ' . $tab[$i] . "\n";
        }
    }
}
$leTableau = [ 4, 23, 45, 1, 2 ];
$tlt = new TriLeTableau();
$tlt->affiche($leTableau);
$tlt->tri($leTableau);
echo "------------\n";
$tlt->affiche($leTableau);
