<?php

class TriLeTableau{

    public array $tab;

    public function __construct(array $tab){
        $this->checkLeTableau($tab);
        $this->tab = $tab;
    }

    private function checkLeTableau(array $tab){
        for ($i = 0; $i < sizeof($tab); $i++) {
            if(!is_numeric($tab[$i])){
                throw new Exception("Stop ça !");
            }
        }
    }

    public function tri(){
        sort($this->tab);
        return $this;
    }

    public function affiche(){
        for ($i = 0; $i < sizeof($this->tab); $i++) {
            echo $i . ' : ' . $this->tab[$i] . "\n";
        }
        return $this;
    }

    public function elSeparator(){
        echo "--------\n";
        return $this;
    }

}
$leTableau = [ 4, 23, 45, 1, 2 ];
try{
    $tlt = new TriLeTableau($leTableau);
    $tlt->affiche()->tri()->elSeparator()->affiche();
} catch(Exception $e){
    echo $e->getMessage();
}
