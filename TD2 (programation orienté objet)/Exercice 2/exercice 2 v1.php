<?php

class TriLeTableau{

    public array $tab;

    public function __construct(array $tab){
        $this->tab = $tab;
    }

    public function tri(){
        sort($this->tab);
    }

    public function affiche(){
        for ($i = 0; $i < sizeof($this->tab); $i++) {
            echo $i . ' : ' . $this->tab[$i] . "\n";
        }
    }
}

$tlt = new TriLeTableau([4, 23, 45, 1, 2]);
$tlt->affiche();
$tlt->tri();
echo "------------\n";
$tlt->affiche();
