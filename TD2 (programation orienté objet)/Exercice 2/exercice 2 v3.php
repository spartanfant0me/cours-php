<?php

class TriLeTableau{
    public static function tri(array &$tab){
        sort($tab);
    }

    public static function affiche(array &$tab){
        for ($i = 0; $i < sizeof($tab); $i++) {
            echo $i . ' : ' . $tab[$i] . "\n";
        }
    }
}
$leTableau = [ 4, 23, 45, 1, 2 ];
TriLeTableau::affiche($leTableau);
TriLeTableau::tri($leTableau);
echo "------------\n";
TriLeTableau::affiche($leTableau);
