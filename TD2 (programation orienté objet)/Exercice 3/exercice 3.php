<?php
class Calculette{
    public $nb1,$nb2;
    public function __construct($nb1,$nb2){
        $this->nb1 = $nb1;
        $this->nb2 = $nb2;
    }
    public function addition(){
        return $this->nb1 + $this->nb2;
    }
    public function soustraction(){
        return $this->nb1 - $this->nb2;
    }
    public function multiplication(){
        return $this->nb1 * $this->nb2;
    }
    public function division(){
        return $this->nb1 / $this->nb2;
    }
}
$c = new Calculette(2, 5);
echo $c->addition(); // affiche 7
echo "\n";
echo $c->multiplication(); // affiche 10
echo "\n";
