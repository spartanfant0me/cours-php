## Exercice 3 :

### Question :
Écrivez une classe qui prend deux nombres en paramètres puis ajoutez-y les méthodes
addition, soustraction, multiplication et division :
`````php
$c = new Calculette(2, 5);
echo $c->addition(); // affiche 7
echo $c->multiplication(); // affiche 10
`````
