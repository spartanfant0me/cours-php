## Exercice 6 :

Écrivez les classes suivantes qui possèdent toute une méthode « getMarque() » qui renvoie
la marque de la voiture :
• Renault
• Peugeot
• Toyota
• Bmw
• Mercedes
• …
````php
$b = new Bmw()
echo $b->getMarque() // affiche ‘C’est une Bmw’
$r = new Renault()
echo $r->getMarque() // affiche ‘C’est une Renault
````
### Questions :

1. Utilisez l’héritage pour pouvoir créer un tableau de voitures
2. Créez une fonction qui prend en paramètre un chiffre et qui renvoie un tableau
   contenant le même nombre de voitures. Vous pouvez utilisez la fonction ```rand()```
   pour générer aléatoirement ce tableau (avec des marques différentes).
