<?php
abstract class Voiture {

    const MARQUES = [
        Bmw::class,
        Audi::class,
        Renault::class
    ];

    public function getMarque() : string{
        return static::class;
    }

}
