<?php
include 'Voiture.php';
include 'Bmw.php';
include 'Audi.php';
include 'Renault.php';

main();
function main(){
    $voitures = generateRandomVoitures();
    foreach($voitures as $voiture){
        echo $voiture->getMarque()."\n";
    }
}

function generateRandomVoitures() : array{
    $rand = rand(2, 10);
    $voitures = [];
    for($i=0; $i < $rand; $i++){
        $marque = Voiture::MARQUES[
        rand(0,sizeof(Voiture::MARQUES)-1)];
        $voitures[] = new $marque();
    }
    return $voitures;
}
