<?php
main();
function main(){
    $tableau = [1,2,3,4,5,6];
    // retourne un tableau avec chaque valeur dans dans le premier tableau passé dans la fonction
    print_r(array_map('multiplierParDeux',$tableau)); // retourne 2,4,6,8,10,12
}
function multiplierParDeux($valeur){
    return $valeur * 2;
}