<?php

class Image {

    public $filename;
    public $greyLevel;
    public $magikNumber;
    public $width, $height;
    public $pixels = [];

}