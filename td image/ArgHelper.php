<?php

class ArgHelper {

    const TFT_H = 0;
    const TFT_V = 1;

    const TRANSFORMATIONS = [
        '-h' => self::TFT_H,
        '-v' => self::TFT_V
    ];

    private int $transformation;
    private string $filepath;
    private array $argv;

    public function __construct(array $argv)
    {
        $this->argv = $argv;
        $this->parse();
    }

    public function parse(){
        array_splice($this->argv, 0, 1);
        array_walk($this->argv, function($item){
            if(str_starts_with($item, '-') && array_key_exists($item, self::TRANSFORMATIONS)){
                $this->transformation = self::TRANSFORMATIONS[$item];
            }else {
                $this->filepath = $item;
            }
        });
    }

    public function isHorizontal(){
        return $this->transformation == self::TFT_H;
    }

    public function isVertical(){
        return $this->transformation == self::TFT_V;
    }

    public function getFilepath(){
        return $this->filepath;
    }

}