<?php

class ImageHelper
{

    const SUPPORTED_FORMAT = [
        'image/jpg',
        'image/jpeg',
        'image/png',
        'image/tiff',
        'image/bmp'
    ];

    const DEFAULT_OUTPUT_FORMAT = '.jpg';

    public static function getImage($src) : Image
    {
        $pgm = self::toPgm($src);
        $content = file_get_contents($pgm);
        $contentArray = explode("\n", $content);
        $image = new Image();
        $image->filename = pathinfo($src, PATHINFO_FILENAME);
        $image->magikNumber = $contentArray[0];
        $image->width = explode(' ', $contentArray[1])[0];
        $image->height = explode(' ', $contentArray[1])[1];
        $image->greyLevel = $contentArray[2];
        array_splice($contentArray, 0, 3);
        $laLigneDePixels = trim(implode(' ', $contentArray), ' ');
        $pixelArray = explode(' ', $laLigneDePixels);
        for($i = 0; $i < $image->width; $i++){
            for($j = 0; $j < $image->height; $j++){
                $image->pixels[$i][$j] = $pixelArray[$i + 1 * $j + 1];
            }
        }
        /*$pixelArray = array_filter($pixelArray, function($item){
            return !empty($item);
        });*/
        $image->pixels = array_chunk($pixelArray, $image->width);
        return $image;
    }

    public static function toImage(Image $image){
        $output = FileHelper::DIR . DIRECTORY_SEPARATOR . $image->filename . '.pgm';
        $content = $image->magikNumber . "\n";
        $content .= $image->width . ' ' . $image->height . "\n";
        $content .= $image->greyLevel . "\n";
        for($i = 0; $i < $image->width; $i++){
            for($j = 0; $j < $image->height; $j++){
                if(!isset($image->pixels[$i][$j])){
                    $content .= "0 ";
                    continue;
                }
                $content .= $image->pixels[$i][$j];
                if(($i + 1 * $j + 1) % $image->width == 0){
                    $content .= "\n";
                    continue;
                }
                $content .=  ' ';
            }    
        }
        file_put_contents($output, $content);
        self::fromPgm($output);
    }

    public static function symV(Image &$image){
        for($i = 0; $i < $image->height; $i++){
            $image->pixels[$i] = array_reverse($image->pixels[$i]);
        }
    }

    private static function toPgm($src): string
    {
        $imageLocale = FileHelper::copyFileInTmp($src);
        if (!in_array(mime_content_type($imageLocale), self::SUPPORTED_FORMAT)) {
            echo "Not supported\n";
            FileHelper::delFile($imageLocale);
            exit(1);
        }
        $leNomPgm = pathinfo($imageLocale, PATHINFO_FILENAME) . '.pgm';
        $leDir = pathinfo($imageLocale, PATHINFO_DIRNAME);
        $pgmFilepath = $leDir . DIRECTORY_SEPARATOR . $leNomPgm;
        $cmd = 'convert ' . $imageLocale . ' -compress none ' . $pgmFilepath;
        exec($cmd, $output, $ret);
        if ($ret != 0) {
            echo "La sauce\n";
            FileHelper::delFile($imageLocale);
            exit(1);
        }
        return $pgmFilepath;
    }

    private static function fromPgm($src): string
    {
        $leNom = pathinfo($src, PATHINFO_FILENAME) . self::DEFAULT_OUTPUT_FORMAT;
        $leDir = pathinfo($src, PATHINFO_DIRNAME);
        $filepath = $leDir . DIRECTORY_SEPARATOR . $leNom;
        $cmd = 'convert ' . $src . ' ' . $filepath;
        exec($cmd, $output, $ret);
        if ($ret != 0) {
            echo "La sauce\n";
            FileHelper::delFile($filepath);
            exit(1);
        }
        return $filepath;
    }

}
