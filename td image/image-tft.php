<?php

include 'ArgHelper.php';
include 'ImageHelper.php';
include 'Image.php';

main($argv);

function main(array $argv){
    $ah = new ArgHelper($argv);
    $image = ImageHelper::getImage($ah->getFilepath());
    ImageHelper::toImage($image);
}