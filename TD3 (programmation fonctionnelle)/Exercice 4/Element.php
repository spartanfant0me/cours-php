<?php

abstract class Element {

    public string $filename;

    public function __construct(string $filename){
        $this->filename = rtrim($filename, DIRECTORY_SEPARATOR);
    }

    public static function getElement(string $filename) : Element
    {
        if(is_link($filename)){
            return new Link($filename);
        }
        if(is_dir($filename)){
            return new Folder($filename);
        }
        return new File($filename);
    }

}