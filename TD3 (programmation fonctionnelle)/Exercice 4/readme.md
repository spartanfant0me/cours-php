## Exercice 4 :

Écrivez une fonction qui prend en paramètre un chemin et qui stocke l’arborescence à partir
de ce chemin.
Utilisez des objets pour représenter les différents éléments de l’arborescence (fichiers,
dossiers, liens symboliques, etc…).
Écrivez du code qui permet d’afficher cette arborescence.