<?php

include 'Element.php';
include 'File.php';
include 'Link.php';
include 'Folder.php';

main($argv);

function main($argv){
    $path = sizeof($argv) < 2 ? exit(1) : $argv[1];
    if(!is_dir($path)){
        exit(1);
    }
    $elements = new Folder($path);
    var_dump($elements);
}