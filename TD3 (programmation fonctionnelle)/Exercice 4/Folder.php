<?php

class Folder extends Element {

    public array $elements = [];

    public function __construct(string $filename)
    {
        parent::__construct($filename);
        $this->elements = $this->browse();
    }

    private function browse() : array
    {
        $elements = @scandir($this->filename);
        array_splice($elements, 0, 2);
        if(is_bool($elements)){
            return [];
        }
        return array_map(function($filename){
            $path = $this->filename . DIRECTORY_SEPARATOR . $filename;
            echo $path."\n";
            return Element::getElement($path);
        }, $elements);
    }

}