<?php

main($argv);

function main($argv){
    $numbers = getNumbers($argv);
    array_walk($numbers, 'affiche');
}

function affiche(int $number)
{
    echo "Le nombre $number est ";
    echo isPair($number) ? "pair\n" : "impair\n";
}
function getNumbers(array $argv) : array
{
    if(sizeof($argv) < 2){
        echo "Le tableau mon reuf ?!?\n";
        exit(1);
    }
    if(str_contains($argv[1], ',')){
        return explode(',', $argv[1]);
    }
    return [$argv[1]];
}

function isPair(int $number){
    return $number % 2 == 0;
}