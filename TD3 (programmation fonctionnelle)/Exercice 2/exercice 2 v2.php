<?php

main($argv);

function main($argv)
{
    echo affiche1Ligne(queLesPairs(getNumbers($argv)), 'Les nombres pairs sont : ');
}

function affiche1Ligne(array $numbers, string $leDebut): string
{
    return rtrim(
            array_reduce(
                $numbers,
                function ($leTexte, $leNumber) {
                    return $leTexte . $leNumber . ', ';
                },
                $leDebut
            ),
            ", "
        ) . "\n";
}

function queLesPairs(array $numbers)
{
    return array_filter($numbers, 'isPair');
}

function affichePairOuImpair(int $number)
{
    echo "Le nombre $number est ";
    echo isPair($number) ? "pair\n" : "impair\n";
}

function getNumbers(array $argv): array
{
    if (sizeof($argv) < 2) {
        echo "Le tableau mon reuf ?!?\n";
        exit(1);
    }
    if (str_contains($argv[1], ',')) {
        return explode(',', $argv[1]);
    }
    return [$argv[1]];
}

function isPair(int $number)
{
    return $number % 2 == 0;
}