## Exercice 1 :

Écrivez une fonction qui prend en paramètre un tableau de nombres et qui retourne un
tableau avec seulement les nombres pairs.
N’oubliez pas d’isoler au maximum le code impur et d’utiliser au moins une fonction d’ordre
supérieur.