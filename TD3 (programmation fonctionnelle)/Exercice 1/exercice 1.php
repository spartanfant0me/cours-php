<?php

main($argv);

function main($argv){
    $numbers = getNumbers($argv);
    array_walk($numbers, 'affichePrime');
}

function affichePrime($number){
    echo $number. ' il ';
    echo isPrime($number) ? 'est ' : 'est pas ';
    echo "premier\n";
}

function getNumbers(array $argv) : array
{
    if(sizeof($argv) < 2){
        echo "Le tableau mon reuf ?!?\n";
        exit(1);
    }
    if(str_contains($argv[1], ',')){
        return explode(',', $argv[1]);
    }
    return [$argv[1]];
}

function isPrime(int $number): bool
{
    if ($number == 0 || $number == 1) {
        return FALSE;
    }
    if ($number == 2 || $number == 3 || $number == 5 || $number == 7) {
        return TRUE;
    }
    return !($number % 2 == 0 || $number % 3 == 0 || $number % 5 == 0 || $number % 7 == 0);
}