# Lien utils :

- ### [Tuto sur la programmation fonctionnelle](https://www.maxpou.fr/programmation-fonctionnelle-php)


# Notions interessantes :

## TD1 :
- ### [Exercice 3](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD1%20(programmation%20s%C3%A9quentielle)/Exercice%203?ref_type=heads) : Première intégration des argument en ligne de commande avec vérification.
- ### [Exercice 6](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD1%20(programmation%20s%C3%A9quentielle)/Exercice%206?ref_type=heads) : Arguments en ligne de commande avec 2 paramètres de types différents (string / int).
- ### [Exercice 7-8](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD1%20(programmation%20s%C3%A9quentielle)/Exercice%207-8?ref_type=heads) : Première implémentation d'une boucle ````while()````.
- ### [Exercice 9](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD1%20(programmation%20s%C3%A9quentielle)/Exercice%209?ref_type=heads) : Boucle ```while()``` avec opérateur "ou".
- ### [Exercice 11-12](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD1%20(programmation%20s%C3%A9quentielle)/Exercice%2011-12?ref_type=heads) : Recher de valeurs dans un tableau.
- 
## TD2 :
- ### [Exercice 1](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD2%20(programation%20orient%C3%A9%20objet)/Exercice%201?ref_type=heads) : Première initialisation de class avec constructeur.
- ### [Exercice 2 V1](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD2%20(programation%20orient%C3%A9%20objet)/Exercice%202?ref_type=heads) : Utilisation de la fonction ```Sort()``` et **méthodes** dans une class.
- ### [Exercice 2 V2](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD2%20(programation%20orient%C3%A9%20objet)/Exercice%202?ref_type=heads) : Transformation de la class pour lui passer des arguments **par référence**.
- ### [Exercice 2 V3](hhttps://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD2%20(programation%20orient%C3%A9%20objet)/Exercice%202?ref_type=heads) : Passage des méthodes en ```static```.
- ### [Exercice 2 V4](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD2%20(programation%20orient%C3%A9%20objet)/Exercice%202?ref_type=heads) : Ajout de ```return $this``` pour pouvoir enchainer les **méthodes**.
- ### [Exercice 2 V5](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD2%20(programation%20orient%C3%A9%20objet)/Exercice%202?ref_type=heads) : Ajout de la structure ```try/catch``` pour gérer les **exceptions** (erreurs).
- ### [Exercice 4](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD2%20(programation%20orient%C3%A9%20objet)/Exercice%202?ref_type=heads) : Utilisation de class helper et utilisation de l'opérateur **spaceship** (<=>) pour trier une liste.
- ### [Exercice 5 V1 et V2](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD2%20(programation%20orient%C3%A9%20objet)/Exercice%202?ref_type=heads) : Première utilsation de l'**héritage**
- ### [Exercice 6](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD2%20(programation%20orient%C3%A9%20objet)/Exercice%206?ref_type=heads): Utilsation du "**late static binding**"

## TD3 :
- ### [Exercice 1](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD3/Exercice%201?ref_type=heads) : Utilisation de ```array_walk()``` et ```explode()```
- ### [Exercice 2 V2](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD3/Exercice%202?ref_type=heads) : Utilisation de ```str_contains()``` et de **fonction d'ordre supéieur**.
- ### [Exercice 3](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD3/Exercice%203?ref_type=heads) : Utilisation de fonction **recursive**.
- ### [Exercice 4](https://gitlab.com/spartanfant0me/cours-php/-/tree/master/TD3/Exercice%204?ref_type=heads) : Utilisation de ```rtrim()```, ```array_splice```, ```scandir()```, ```DIRECTORY_SEPARATOR``` dans de la programmation objet.
