<?php
main();
function main(){
    // cette fonction est d'ordre supérieur car elle appelle une fonction dans ses paramètres
    echo multiplierParDeux(auCarre(4));
}
function multiplierParDeux($valeur){
    return $valeur * 2;
}
function auCarre($valeur){
    return $valeur * $valeur;
}