<?php

print 1 > 2; // Vérifie si 1 et supérieur a 2
print 1 < 2; // Vérifie si 1 et inférieur a 2
print 1 >= 2; // Vérifie si 1 et supérieur ou égale a 2
print 1 <= 2; // Vérifie si 1 et inférieur ou égale a 2
print 1 == 2; // Vérifie si 1 et égale a 2
print 1 === 2; // Vérifie si 1 et identique (égale et du même type) a 2
print 1 != 2; // Vérifie si 1 et pas égale a 2
print 1 !== 2; // Vérifie si 1 et pas identique (égame et du même type) a 2
print 1 <=> 2; // Vérifie si compare 1 et 2, retourne 1 si la valeur de gauche est plus grande, 0 si ils sont égaux et -1 si la valeur de droite est plus grande
print !true; // Inverse la valeur (true -> false)