<?php
// Exemple repris de l'exercice 3 du TD3
main($argv);

function main($argv){
    $number = sizeof($argv) < 2 ? exit(1) : $argv[1];
    echo 'Le factoriel de '.$number.' est : '. factoriel($number)."\n";
}

function factoriel(float $number) : float
{
    if($number == 0){
        return 1;
    }
    return $number * factoriel($number - 1); // recursivité car on rappelle la fonction dans elle même
}