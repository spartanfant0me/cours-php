## Exercice 3 :

### Question :
Écrire un programme qui convertit un montant en fonction d’un taux de change. Les
modalités d’affichage sont les suivantes :

```
Entrez le montant a convertir : 12
Le montant a convertir est : 12.000000
Entrez le taux de change a appliquer : 2
Le taux de change a appliquer est : 2.000000
Le montant converti est : 24.000000
```
