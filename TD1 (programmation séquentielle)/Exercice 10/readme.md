## Exercice 10 :

### Question :
Écrire un programme qui demande un nombre de départ, et qui calcule la somme des
entiers jusqu’à ce nombre. Par exemple, si l’on entre 5, le programme doit calculer : 1 + 2 + 3
+ 4 + 5 et afficher 15.
