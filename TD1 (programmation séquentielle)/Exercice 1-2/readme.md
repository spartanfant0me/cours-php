## Exercice 1 et 2 :

### Question 1 :
Écrire un programme qui demande la saisie d’une lettre de l’alphabet et qui affiche la lettre
majuscule correspondante.

### Question 2 :
Modifier le programme précédent afin qu’il vérifie que le caractère saisi au clavier est bien
une lettre minuscule. Dans un second temps, on pourra modifier encore le programme pour
que celui-ci affiche la lettre minuscule correspondant à une lettre majuscule qui aurait été
saisie.
