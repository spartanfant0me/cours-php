<?php
main($argv);
function main(array $argv){
    $valeur = getArgValue($argv, "-v");
    while ($valeur < 0) {
        $valeur = readline("Saisie incorrecte. Entrez un valeur positive: ");
    }
    echo "Le nombre saisi est $valeur\n";
}

function getArgValue($argv, $argument){
    for ($i = 1; $i < sizeof($argv); $i++) {
        if ($argv[$i] == $argument) {
            if (!isset($argv[$i + 1])) {
                echo "Valeur manquante après $argument";
                exit(1);
            }
            if (!is_numeric($argv[$i + 1])) {
                echo "Il faut indiquer une valeur numérique";
                exit(1);
            }
            return $argv[$i + 1];
        }
    }
    echo "Argument maquant. Argument attendu: $argument";
    exit(1);
}
