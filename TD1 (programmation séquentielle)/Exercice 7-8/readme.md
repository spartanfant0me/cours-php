## Exercice 7 :

### Question 1 :
On souhaite réaliser un programme qui permet de saisir un nombre positif. Une vérification
sera effectuée afin de refuser les nombres négatifs :
```
Saisissez un nombre positif : -5
Saisissez un nombre positif : -3
Saisissez un nombre positif : 2
Le nombre saisi est 2.
```
### Question 2 :
On souhaite améliorer le programme précédent afin que le message ne soit pas le même
lors de la demande suite à une erreur de saisie :
```
Saisissez un nombre positif : -5
Saisie incorrecte, recommencez votre saisie : -3
Saisie incorrecte, recommencez votre saisie : 2
Le nombre saisi est 2.
```
