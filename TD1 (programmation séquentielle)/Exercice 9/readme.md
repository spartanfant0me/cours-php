## Exercice 9 :

### Question :
On souhaite réaliser un programme qui demande un nombre compris entre 10 et 20, jusqu’à
ce que la réponse convienne. En cas de réponse supérieure à 20, on fera apparaître un
message : « Plus petit ! », et inversement, « Plus grand ! » si le nombre est inférieur à 10.
