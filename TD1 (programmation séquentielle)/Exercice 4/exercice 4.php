<?php
main($argv);
function main(array $argv){
    $masse = getArgValue($argv,"-m");
    $taille = getArgValue($argv, "-t");

    echo "Votre IMC est de ".($masse/($taille*$taille)).".\n";
}
function getArgValue($argv, $argument){
    for ($i = 1; $i < sizeof($argv); $i++) {
        if ($argv[$i] == $argument) {
            if (!isset($argv[$i + 1])) {
                echo "Valeur manquante après $argument";
                exit(1);
            }
            if (!is_numeric($argv[$i + 1])) {
                echo "Il faut indiquer une valeur numérique";
                exit(1);
            }
            return $argv[$i + 1];
        }
    }
    echo "Argument maquant. Argument attendu: $argument";
    exit(1);
}

