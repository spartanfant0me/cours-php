## Exercice 10 :

### Question 1 :
Écrire un programme qui demande successivement 5 nombres à l’utilisateur, et qui indique
lequel de ces nombres est le plus grand.

### Question 2 :
Modifiez le programme précédent pour qu’il affiche aussi la position de ce nombre dans la
séquence de nombres saisis.
