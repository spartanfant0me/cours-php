## Exercice 5 :

### Question :
Écrire un programme qui demande la somme d’argent souhaitée et détermine si le retrait
est autorisé ou non. Le retrait est refusé dans le cas où la somme demandée dépasse 100. Le
message sera adapté selon la situation : Retrait accepté ou Retrait refusé.

```
Bonjour, entrez la somme demandee : 130
La somme demandee est : 130
Retrait refuse
```
