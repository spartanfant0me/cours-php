<?php
main($argv);
function main(array $argv){
    $montant = getArgValue($argv, "-m");

    echo "La somme demandee est: $montant \n";

    if($montant > 100){
        echo "Retrait refuse\n";
    }
    else{
        echo "Retrait accepte\n";
    }
}
function getArgValue($argv, $argument){
    for ($i = 1; $i < sizeof($argv); $i++) {
        if ($argv[$i] == $argument) {
            if (!isset($argv[$i + 1])) {
                echo "Valeur manquante après $argument";
                exit(1);
            }
            if (!is_numeric($argv[$i + 1])) {
                echo "Il faut indiquer une valeur numérique";
                exit(1);
            }
            return $argv[$i + 1];
        }
    }
    echo "Argument maquant. Argument attendu: $argument";
    exit(1);
}
