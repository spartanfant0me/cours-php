<?php

$tableau = [1,2,3,4,5,6];
// supprime les valeurs d'un tableau a partir d'une position et pour un certains nombre (dans l'exemple retire 2 valeurs a partir de la position 0)
array_splice($tableau,0,2); // retire 1,2
print_r($tableau); // retourne 3,4,5,6