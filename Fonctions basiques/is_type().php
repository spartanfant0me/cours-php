<?php
is_array(); // retourne "vrai" si la valeur est un tableau
is_bool(); // retourne "vrai" si la valeur est un booléen
is_callable(); // retourne "vrai" si la valeur est appelable (comme une fonction)
is_countable(); // retourne "vrai" si la valeur est comptable (par exemple un tableau)
is_double(); // retourne "vrai" si la valeur est un "double"
is_float();  // retourne "vrai" si la valeur est "float"
is_int(); // retourne "vrai" si la valeur est un "int"
is_integer(); // meme fonction qu'au dessus
is_iterable();  // retourne "vrai" si la valeur est itérable (comme un tableau)
is_long(); // retourne "vrai" si la valeur est un "long"
is_null();  // retourne "vrai" si la valeur est "null"
is_numeric();  // retourne "vrai" si la valeur est un chiffre / nombre
is_object();  // retourne "vrai" si la valeur est un objet
is_scalar();  // retourne "vrai" si la valeur est un entier / float / string ou booléen
is_string(); // retourne "vrai" si la valeur est une chaine de charactère