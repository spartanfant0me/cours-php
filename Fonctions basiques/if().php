<?php
$valeur1 = 4;
$valeur2 = 8;
$valeur3 = 16;

// si $valeur1 est égale à $valeur2 execute son code
if ($valeur1 == $valeur2){ // utilise les opérateurs classiques
    echo 'égaux'; // s'execute seulement si la condition au dessus est vrai
}

if ($valeur1 == $valeur2 || $valeur1 > $valeur2){ // si la valeur 1 est égale a la valeur 2 OU si valeur 1 est supérieur a la valeur 2
    // code
}

if ($valeur1 == $valeur2 && $valeur2 > $valeur3){ // si la valeur 1 est égale a la valeur 2 ET si valeur 2 est supérieur a la valeur 3
    // code
}
