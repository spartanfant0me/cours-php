<?php
$input = '1,2,3,4,5,6, ';
// retire le charactère spécifié a partir de la droite
echo rtrim($input,', ')."\n";

// retire le charactère spécifié a partir de la gauche
echo ltrim($input,'1')."\n";

// retire le charactère spécifié a partir de la gauche et la droite
echo trim($input,', ');