<?php
$tableau = [1,2,3,4,5,6,7,8,9];

// pour chaque valeur de $tableau en tant que $valeur -> execute le code
foreach ($tableau as $valeur){
    echo $valeur; // ecrit chaque valeur du tableau, une par une (donc 1,2,3,4,5,6,7,8,9)
}