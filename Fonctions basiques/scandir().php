<?php

// retourne un tableau contenant les fichiers et dossier dans un dossier donnée
print_r(scandir('/var'));

/* Exemple de sortie pour scandir du dossier /var
Array
(
    [0] => .  inutile(a supprimer avec array_splice)
    [1] => .. inutile(a supprimer avec array_splice)
    [2] => .updated
    [3] => adm
    [4] => agentx
    [5] => cache
    [6] => crash
    [7] => games
    [8] => lib
    [9] => lock
    [10] => log
    [11] => mail
    [12] => opt
    [13] => run
    [14] => spool
    [15] => tmp
)
*/