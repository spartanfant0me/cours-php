<?php
ctype_alnum(); //Vérifie qu'une chaîne est alphanumérique
ctype_alpha();  //Vérifie qu'une chaîne est alphabétique
ctype_cntrl();  //Vérifie qu'un caractère est un caractère de contrôle
ctype_digit();  //Vérifie qu'une chaîne est un entier
ctype_graph();  //Vérifie qu'une chaîne est imprimable
ctype_lower();  //Vérifie qu'une chaîne est en minuscules
ctype_print();  //Vérifie qu'une chaîne est imprimable
ctype_punct();  //Vérifie qu'une chaîne contient de la ponctuation
ctype_space();  //Vérifie qu'une chaîne n'est faite que de caractères blancs
ctype_upper();  //Vérifie qu'une chaîne est en majuscules
ctype_xdigit(); // Vérifie qu'un caractère représente un nombre hexadécimal